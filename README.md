# Hướng dẫn chạy project Add 2 number

## Project được viết bằng javascript, nodejs và thư viện test là jest

### Cấu trúc project

- File chính của project là index.js chứa module MyBigNumber
- Thư mục test chứa file test case add2num.test.js

### Những điều lưu ý khi chạy project

1. PC/laptop đã cài đặt NodeJs và npm
2. Clone project về máy bằng cách mở terminal trên máy tại thư mục mà ta muốn lưu sau đó chạy lệnh:
   > git clone https://gitlab.com/vqtruong0910/add2number.git  
   > cd add2number  
   > npm i
3. Thay đổi giá trị mà ta muốn cho vào các biến mà ta muốn
4. Chạy lệnh để ra kết quả
   > node index.js
5. Để chạy các test case chạy lệnh
   > npm run test
