const MyBigNumber = require('../index')
describe('Test sum function of MyBigNumber class', () => {
  test('Test case sum 1', () => {
    const obj = new MyBigNumber()
    expect(obj.sum('1', '1')).toBe(2)
  })

  test('Test case sum 2', () => {
    const obj = new MyBigNumber()
    expect(obj.sum('0', '0')).toBe(0)
  })

  test('Test case sum 3', () => {
    const obj = new MyBigNumber()
    expect(obj.sum('123', '456')).toBe(579)
  })

  test('Test case sum 4', () => {
    const obj = new MyBigNumber()
    expect(obj.sum('999999', '1')).toBe(1000000)
  })

  test('Test case sum 5', () => {
    const obj = new MyBigNumber()
    expect(obj.sum('1234', '897')).toBe(2131)
  })

  test('Test case sum 6', () => {
    const obj = new MyBigNumber()
    expect(obj.sum('896', '1234')).toBe(2130)
  })
})
