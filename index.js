class MyBigNumber {
  sum(stn1, stn2) {
    // chuyển 2 chuỗi thành dạng mảng
    const stn1ToArray1 = stn1.split('')
    const stn2ToArray2 = stn2.split('')
    // biến tạm để lưu lại số dư cần nhớ
    let tmp = 0
    // Mảng lưu kết quả sau mỗi lần chạy và nạp vào
    let result = []
    // kiểm tra 2 mảng còn dữ liệu không nếu không kết thúc vòng lặp
    while (stn1ToArray1.length !== 0 || stn2ToArray2.length !== 0) {
      // lấy dữ liệu cuối mảng rồi chuyển về dạng int
      const number1 = Number(stn1ToArray1.pop())
      const number2 = Number(stn2ToArray2.pop())
      // kiểm tra nết trường hợp 1 trong 2 mảng đã hết nhưng mảng còn lại còn dữ liệu thì khi chạy sẽ ra số 0
      const checkNumber1 = !isNaN(number1) ? number1 : 0
      const checkNumber2 = !isNaN(number2) ? number2 : 0
      // cộng 2 số lấy ra và cộng thêm phần dư
      const total = checkNumber1 + checkNumber2 + tmp
      // trường hợp lớn hơn 10 sẽ sinh ra phần số dư trước nó ví dụ là 1 và ngược lại sẽ dư ra 0, thực hiện tính toán và nạp vào mảng
      if (total >= 10) {
        result.unshift(total % 10)
        tmp = Math.floor(total / 10)
        console.log(`Lưu ${total % 10} vào kết quả và nhớ ${tmp}`)
        console.log('Kết quả mới là ', result.join(''))
      } else {
        result.unshift(total)
        tmp = 0
        console.log(`Lưu ${total} vào kết quả và nhớ ${tmp}`)
        console.log('Kết quả mới là ', result.join(''))
      }
    }
    if (tmp !== 0) result.unshift(tmp)
    return Number(result.join(''))
  }
}
const obj = new MyBigNumber()
// Thay đổi giá trị mà ta mong muốn ở đây
const value1 = '99999'
const value2 = '1'

console.log(obj.sum(value1, value2))
module.exports = MyBigNumber
